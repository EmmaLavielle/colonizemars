<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeclarationRepository")
 */
class Declaration
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nameArea;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     */
    private $nameOre;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive
     */
    private $quantityOre;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive
     */
    private $basicDanger;

    /**
     * @ORM\Column(type="datetime")
     */
    private $CreatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameArea(): ?string
    {
        return $this->nameArea;
    }

    public function setNameArea(string $nameArea): self
    {
        $this->nameArea = $nameArea;

        return $this;
    }

    public function getNameOre(): ?string
    {
        return $this->nameOre;
    }

    public function setNameOre(string $nameOre): self
    {
        $this->nameOre = $nameOre;

        return $this;
    }

    public function getQuantityOre(): ?int
    {
        return $this->quantityOre;
    }

    public function setQuantityOre(int $quantityOre): self
    {
        $this->quantityOre = $quantityOre;

        return $this;
    }

    public function getBasicDanger(): ?int
    {
        return $this->basicDanger;
    }

    public function setBasicDanger(int $basicDanger): self
    {
        $this->basicDanger = $basicDanger;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->CreatedAt;
    }

    public function setCreatedAt(\DateTimeInterface $CreatedAt): self
    {
        $this->CreatedAt = $CreatedAt;

        return $this;
    }
}
