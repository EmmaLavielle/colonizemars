<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Declaration;
use App\Repository\DeclarationRepository;
use App\Form\DeclarationType;

class EpidemicController extends AbstractController
{
    /**
     * @Route("/", name="epidemic")
     */
    public function index(DeclarationRepository $repo)
    {        
        $declarations = $repo->findAll();

        return $this->render('epidemic/index.html.twig', [
            'controller_name' => 'EpidemicController',
            'declarations' => $declarations
        ]);
    }

    /**
     * @Route("/epidemic/new", name="epidemic_create")
     * @Route("/epidemic/{id}/edit", name="epidemic_edit")
     */
    public function form(Declaration $declaration = null, Request $request, ObjectManager $manager)
    {         
        if(!$declaration)
        {
            $declaration = new Declaration();
        }

        $form = $this->createForm(DeclarationType::class, $declaration);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            if(!$declaration->getId())
            {
                $declaration->setCreatedAt(new \DateTime());
            }

            $manager->persist($declaration);
            $manager->flush();

            return $this->redirectToRoute('epidemic_show', ['id' => $declaration->getId()]);
        }

        return $this->render('epidemic/create.html.twig', [
            'formDeclaration' => $form->createView(),
            'editMode' => $declaration->getId() !== null,
        ]);
    }

    /**
     * @Route("/epidemic/{id}", name="epidemic_show")
     */
    public function show(Declaration $declaration)
    {
        return $this->render('epidemic/show.html.twig', [
            'declaration' => $declaration
        ]);
    }
}
