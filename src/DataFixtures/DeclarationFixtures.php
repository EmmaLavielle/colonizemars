<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Declaration;

class DeclarationFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 9; $i++)
        {
            $declaration = new Declaration();
            $declaration->setNameArea("Nom de la zone n°$i")
                        ->setNameOre("Nom du minerai n°$i")
                        ->setQuantityOre($i)
                        ->setBasicDanger($i)
                        ->setCreatedAt(new \DateTime());
            $manager->persist($declaration);
        }

        $manager->flush();
    }
}
