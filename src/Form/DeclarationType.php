<?php

namespace App\Form;

use App\Entity\Declaration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class DeclarationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameArea', TextType::class, [
                'label' => 'Nom de la zone'
            ])
            ->add('nameOre', ChoiceType::class, [
                'label' => 'Nom du minerai',
                'choices' => [
                    'Klingon' => "Klingon",
                    'Chomdû' => "Chomdû",
                    'Perl' => "Perl",
                    'Other' => "A new ore",
                ],
            ])
            ->add('quantityOre', ChoiceType::class, [
                'label' => 'Quantité de minerai',
                'choices' => [
                    '1' => "1",
                    '2' => "2",
                    '3' => "3",
                ],
            ])
            ->add('basicDanger', ChoiceType::class, [
                'label' => 'Dangerosité de base',
                'choices' => [
                    '1' => "1",
                    '4' => "4",
                    '7' => "7",
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Declaration::class,
        ]);
    }
}
